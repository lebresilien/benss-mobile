import React, {useState, useEffect} from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import MyDrawer from './src/navigation/DrawerNavigator';
import { Provider } from 'react-redux'
import Store from './src/store/configureStore';


export default function App() {

  return (

    <Provider store={Store}> 
      <PaperProvider>
        <MyDrawer />
      </PaperProvider>
    </Provider>
  );
}


