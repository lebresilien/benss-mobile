import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import MenuIn from './../components/MenuIn';
import Home from './../views/Home'
import SignIn from './../views/SignIn'
import Temp from './../views/Temp'
import Password from './../views/Password'
import Forfait from './../views/Forfait'
import Profil from './../views/Profil'
import Information from './../views/Information'
import Rdv from './../views/Rdv'
import RdvDate from '../views/RdvDate'
import RdvTime from '../views/RdvTime'
import RdvElement from '../views/RdvElement'
import ListRdv from '../views/ListRdv'
import DetailsRdv from '../views/DetailsRdv'
import Paiment from '../views/Paiment'
import Patient from '../views/Patient'
import ListPatient from '../views/ListPatient'
import DetailsPatient from '../views/DetailsPatient'
import RdvList from '../views/RdvList'
import RdvOperation from '../views/RdvOperation'
import LastRdv from '../views/LastRdv'
import ListIntervention from '../views/ListIntervention'
import DetailsIntervention from '../views/DetailsIntervention'
import DossierMedical from '../views/DossierMedical'
import Intervention from '../views/Intervention'
import Biometry from '../views/Biometry'
import Medical from '../views/Medical'
import { useSelector} from "react-redux";

const Drawer = createDrawerNavigator();

export default function MyDrawer() {
  const currentUser = useSelector((state) => state)


  return (
    <NavigationContainer>
      <Drawer.Navigator drawerContent={props => <MenuIn phone={currentUser.toggleConnexion.phone ? currentUser.toggleConnexion.phone : '' } name={currentUser.toggleConnexion.name} {...props} />} >
        <Drawer.Screen name="Temp" component={Temp} />
        <Drawer.Screen name="Password" component={Password} />
        <Drawer.Screen name="Forfait" component={Forfait} />
        <Drawer.Screen name="Profil" component={Profil} />
        <Drawer.Screen name="Information" component={Information} />
        <Drawer.Screen name="Rdv" component={Rdv} />
        <Drawer.Screen name="RdvDate" component={RdvDate} />
        <Drawer.Screen name="RdvTime" component={RdvTime} />
        <Drawer.Screen name="RdvElement" component={RdvElement} />
        <Drawer.Screen name="ListRdv" component={ListRdv} />
        <Drawer.Screen name="DetailsRdv" component={DetailsRdv} />
        <Drawer.Screen name="Paiment" component={Paiment} />
        <Drawer.Screen name="Patient" component={Patient} />
        <Drawer.Screen name="ListPatient" component={ListPatient} />
        <Drawer.Screen name="DetailsPatient" component={DetailsPatient} />
        <Drawer.Screen name="RdvList" component={RdvList} />
        <Drawer.Screen name="RdvOperation" component={RdvOperation} />
        <Drawer.Screen name="LastRdv" component={LastRdv} />
        <Drawer.Screen name="ListIntervention" component={ListIntervention} />
        <Drawer.Screen name="DetailsIntervention" component={DetailsIntervention} />
        <Drawer.Screen name="DossierMedical" component={DossierMedical} />
        <Drawer.Screen name="Intervention" component={Intervention} />
        <Drawer.Screen name="Biometry" component={Biometry} />
        <Drawer.Screen name="Medical" component={Medical} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}