import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MyDrawer from './DrawerNavigator';
import { NavigationContainer } from '@react-navigation/native';
import Temp from './../views/Temp';
import SignIn from './../views/SignIn';

const Stack = createStackNavigator();

export default function MyStack() {
  return (
            <NavigationContainer>
              <Stack.Navigator >
                <Stack.Screen name="Temp" component={Temp} />
                
              </Stack.Navigator>
            </NavigationContainer>
  );
}