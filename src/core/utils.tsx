
export const emailValidator = (email: string) => {
    const re = /\S+@\S+\.\S+/;
  
    if (!email || email.length <= 0) return 'Remplissez l\'adresse email.';
    if (!re.test(email)) return 'Ooops! Rentrer une adresse email valide.';
  
    return '';
  };
  
  export const passwordValidator = (password: string) => {
    if (!password || password.length <= 0) return 'Remplissez le mot de passe.';
  
    return '';
  };
  
  export const passwordCompare = (password: string, confirmpassword: string) => {
    if (password != confirmpassword ) return 'les mots de passent ne sont pas identiques.';
  
    return '';
  };

  export const nameValidator = (name: string) => {
    if (!name || name.length <= 0 ) return 'Remplissez le nom.';
  
    return '';
  };

  export const phoneValidator = (phone: string) => {
    if (!phone || phone.length <= 0 ) return 'Remplissez le numero de téléphone.';
  
    return '';
  };

  export const passValidator = (password: string) => {
    if (!password || password.length <= 0) return 'Remplissez le mot de passe.';
    if (password.length <= 7) return 'Votre mot de passe doit contenir au moins 8 caracteres.';
  
    return '';
  };

  export const tailleValidator = (taille: string) => {
    if (!taille || taille.length <= 0 ) return 'Remplissez la taille.';
  
    return '';
  }

  export const poidsValidator = (poids: string) => {
    if (!poids || poids.length <= 0 ) return 'Remplissez le poids.';
  
    return '';
  }

  export const tempValidator = (temp: string) => {
    if (!temp || temp.length <= 0 ) return 'Remplissez la temperature.';
  
    return '';
  }

  export const ordonnanceValidator = (ordonnance: string) => {
    if (!ordonnance || ordonnance.length <= 0 ) return 'Remplissez l\'ordonnance.';
  
    return '';
  }

  export const observationValidator = (obervation: string) => {
    if (!obervation || obervation.length <= 0 ) return 'Remplissez les observations.';
  
    return '';
  }
  