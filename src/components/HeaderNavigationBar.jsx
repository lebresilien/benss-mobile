import { Icon } from 'native-base';
import React from 'react';
import { Text, StyleSheet, View, TouchableOpacity, Image } from 'react-native';

export default function HeaderNavigationBar({navigation, name, image}) {
   
    return(

           <View style={styles.container} >
               
               {!image ?
                <View style={{flex:1}}>
                    <View style={styles.head}>
                        <View styles={{alignItems:'flex-start'}}>
                            <TouchableOpacity onPress={() => navigation.openDrawer() }>
                                <Icon name="menu" style={{fontSize: 30, color: 'white'}} />
                            </TouchableOpacity>
                        </View>
                            
                                
                        <View styles={{alignItems:'flex-end'}}>
                            <TouchableOpacity  /* onPress={() => navigation.openDrawer() } */>
                                <Image source={require('./../../assets/logo.png')}  style={{width:35,height:35}} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.body}>
                        <Text style={styles.text}> { name }</Text>
                    </View>
                </View>
                :

                <View style={styles.header}>

                    <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View styles={{alignItems:'flex-start'}}>
                            <TouchableOpacity onPress={() => navigation.openDrawer() }>
                                <Icon name="menu" style={{fontSize: 30, color: '#60bdb2'}} />
                            </TouchableOpacity>
                        </View>
                            
                                
                        <View styles={{alignItems:'flex-end'}}>
                            <TouchableOpacity  /* onPress={() => navigation.openDrawer() } */>
                                <Image source={require('./../../assets/logo.png')}  style={{width:35,height:35}} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{flex:5, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={styles.texte}> { name } </Text>
                        <TouchableOpacity>
                           <Image source={image.image} style={styles.image} />
                        </TouchableOpacity>
                    </View>
                </View>

               }
           </View>

           
    );
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      marginTop:30
    },
    head: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft:10,
        marginRight:10,

    },
    header: {
        flex:1,
        marginLeft:10,
        marginRight:10,
    },
    body: {
        flex:1,
        alignItems: 'center'
    },
    texte:{
        fontWeight: 'bold',
        alignItems: 'center',
        fontSize: 25,
        color: 'black',
        textTransform: 'uppercase'
    },
    text:{
        fontWeight: 'bold',
        alignItems: 'center',
        color: 'white',
        fontSize: 25,
        textTransform: 'uppercase'
    },
    image: {
        height: 150,
        width: 150,
    },
    
  });