import React from 'react';
import { ImageBackground, View, StyleSheet, Text } from 'react-native';
import { Drawer } from 'react-native-paper';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useDispatch, useSelector } from "react-redux";

export default function MenuIn({name, phone, navigation}) {

  const [active, setActive] = React.useState('')
  const dispatch = useDispatch()
  const currentUser = useSelector((state) => state)
  

  const deconnexion = () => {
      /*  setActive('deconnexion')  */
       dispatch({ type: 'CONNEXION' })
       navigation.navigate('Temp') 
  }

  function nav(route){
    setActive(route)
    navigation.navigate(route)
  }

    return(
            <View style={styles.container}>

               <View style={styles.background}>
                  <ImageBackground source={require('./../../assets/signin.png')} style={styles.image}>
                        <View style={styles.display}>
                          <Text style={styles.text}>{name}</Text>
                          <Text style={styles.text}>{phone}</Text>
                        </View>
                  </ImageBackground>
               </View>

               <View style={styles.lien}>
                 
                  <Drawer.Section>

                    <Drawer.Item
                        label="Home"
                        active={active === 'home'}
                        icon={({color, size}) => (
                          <FontAwesome name="home" size={23} color="#60bdb2" />
                      )}
                        onPress={() => nav('Temp')}
                    />
                    {currentUser.toggleConnexion.role === "patient" ? 
                      <Drawer.Item
                          label="Rdvs"
                          active={active === 'rdv'}
                          icon={({color, size}) => (
                              <FontAwesome name="user-graduate" size={23} color="#60bdb2"/>
                          )}
                          onPress={() => nav('Rdv')}
                      /> :
                      <Text></Text>
                    }

                    

                  </Drawer.Section>

                  <Drawer.Section>

                    <Drawer.Item
                        label="Mon compte"
                        active={active === 'account'}
                        icon={({color, size}) => (
                          <FontAwesome name="user-alt" size={23} color="#60bdb2" />
                      )}
                        onPress={() => nav('Profil')}
                    />

                    {/* <Drawer.Item
                        label="Paramètres"
                        active={active === 'parametres'}
                        icon={({color, size}) => (
                            <FontAwesome name="user-cog" size={23} color="#60bdb2" />
                        )}
                        onPress={() => nav('parametres')}
                    /> */}

                    <Drawer.Item
                        label="Déconnexion"
                        active={active === 'deconnexion'}
                        icon={({color, size}) => (
                          <MaterialIcons name="logout" size={23} color="#60bdb2" />
                      )}
                        onPress={deconnexion}
                    />

                  </Drawer.Section>

               </View>  

               <View style={styles.account}>
                 
                </View>

            </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'white',
    },
    background:{
      flex:3,
      backgroundColor:'#60bdb2'
    },
    link:{
       flex:5,
    },
    account:{
      flex:4
    },
    display:{
      flex:1,
      justifyContent: 'flex-end',
      marginBottom: 25,
      paddingLeft:10,
    },
    image: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },
    text: {
      fontWeight: "bold",
      color: 'black'
    }
  });