import React, {useState, useEffect,} from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Alert} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from "react-redux";
import { ActivityIndicator } from 'react-native-paper';
import { Container, Header, Body, List, ListItem, Text, Left, Right, Icon } from 'native-base';


export default function LastRdv() {

    const navigation = useNavigation()
    const currentUser = useSelector((state) => state)
    const[loading, setLoading] = useState(true)
    const[rdvs, setRdvs] = useState([]) 
    let url = ''

    useEffect(() => {
        getLastRdv()
    }) 

    const getLastRdv = async() => {

        if(currentUser.toggleConnexion.role === "infirmier"){
                url = 'https://benss.bensscameroun.com/api/v1/rdvs/nurse/last'
        } 
        else{
                url = 'https://benss.bensscameroun.com/api/v1/rdvs/doctor/last'
        } 

        await fetch(url, {
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            },
          })
          .then(res=>res.json())
          .then(res => {
              setRdvs(res.data)
              setLoading(false)
          })
          .catch((error) => { console.warn(error);  })
    }


    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="derniers rdvs"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

                <SafeAreaView style={styles.SafeAreaView}>
                   <ScrollView >

                       { loading ? 
                            <ActivityIndicator animating={true} color='#60bdb2' /> :

                            <View>
                               {rdvs.length == 0 ? 
                                    <Text style={{textAlign:'center', marginTop:35}}></Text>:
                                    <List>
                                        {rdvs.map((rdv, index) => (
        
                                          currentUser.toggleConnexion.role === "infirmier" ?
                                            <ListItem onPress={() => navigation.navigate('RdvOperation',{id: rdv.id})} key={index}>
                                                <Left>
                                                    <Text>
                                                        { rdv.user.surname ? `${rdv.user.name} ${rdv.user.surname}` : rdv.user.name  }
                                                    </Text>
                                                </Left>
                                                    <Body>
                                                        <Text>{rdv.service.name}</Text>
                                                    </Body>
                                                <Right>
                                                    <Icon name="arrow-forward" />
                                                </Right>
                                            </ListItem>:

                                            <ListItem onPress={() => navigation.navigate('DetailsIntervention',{id: rdv.id})} key={index}>
                                                <Left>
                                                    <Text>
                                                        { rdv.user.surname ? `${rdv.user.name} ${rdv.user.surname}` : rdv.user.name  }
                                                    </Text>
                                                </Left>
                                                    <Body>
                                                        <Text>{rdv.service.name}</Text>
                                                    </Body>
                                                <Right>
                                                    <Icon name="arrow-forward" />
                                                </Right>
                                            </ListItem>
                                           
                                        ))}
                                    </List>
                               }
                            </View>
                       }

                   </ScrollView>
                </SafeAreaView>

            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        backgroundColor: 'white'
    },
    SafeAreaView: {
        flex:1,
       justifyContent: 'center'
    }
});