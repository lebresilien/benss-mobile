import React, {useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { useSelector } from "react-redux";
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { Surface } from 'react-native-paper';

export default function Profil() {

    const navigation = useNavigation()
    const[image, setImage] = useState({image: require('./../../assets/profil.png')})
    const currentUser = useSelector((state) => state)
    
    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="Mon compte"  navigation={navigation} image={image} />
            </View> 

            <View style={styles.content}>
                
                <Surface style={styles.surface}>

                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} onPress = { () => navigation.navigate('Information')}>
                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                        <Text style={styles.texte}>Informations</Text>
                    </TouchableOpacity>
                
                </Surface>

                {currentUser.toggleConnexion.role === "patient" ? 
                    <Surface style={styles.surface}>

                        <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}}  onPress={ () => navigation.navigate('DossierMedical')}>
                            <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                            <Text style={styles.texte}>Dossier Medical</Text>
                        </TouchableOpacity>

                    </Surface>:
                    <Text></Text>
                }

                <Surface style={styles.surface}>

                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} onPress={ () => navigation.navigate('Password')}>
                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                        <Text style={styles.texte}>Changer mot de passe</Text>
                    </TouchableOpacity>

                </Surface>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: 'white',
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    surface: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: -8,
        height: 60,
        width: 300,
        justifyContent: 'center',
        elevation: 4,
        borderRadius: 25,
        marginBottom:10
    },
    texte:{
        fontSize: 17,
        color: 'black',
    },
  
});