import React, {useEffect, useState} from 'react';
import { View, Text, StyleSheet, Image,KeyboardAvoidingView,Platform, Alert} from 'react-native';
import Button from '../components/Button'
import TextInput from '../components/TextInput';
import { theme } from '../core/theme';
import { passValidator, emailValidator, nameValidator, phoneValidator } from '../core/utils';
import { ActivityIndicator } from 'react-native-paper';
import { useSelector} from "react-redux";
import { useNavigation } from '@react-navigation/native';
import HeaderNavigationBar from './../components/HeaderNavigationBar';

export default function Patient() {

  const navigation = useNavigation()
  const[loading, setLoading] = useState(false)
  const currentUser = useSelector((state) => state)
  const [name, setName] = useState({ value: '', error: '' });
  const [surname, setSurname] = useState({ value: '', error: '' });
  const [phone, setPhone] = useState({ value: '', error: '' });
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });
  const rule = "patient"

 /*  useEffect(() => {
        setLoading(false)
  }, []) */

  const _onSave = async() => {

    const nameError = nameValidator(name.value);
    const phoneError = phoneValidator(phone.value);
    const emailError = emailValidator(email.value);
    const passwordError = passValidator(password.value);

    if(nameError || phoneError || emailError || passwordError) {
        setName({ ...name, error: nameError });
        setPhone({ ...phone, error: phoneError });
        setEmail({ ...email, error: nameError });
        setPassword({ ...password, error: passwordError });
        return;
    }

    setLoading(true)

    await fetch('https://benss.bensscameroun.com/api/v1/users/nurses/store', {
        method: 'POST',
        headers:{
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
        },
        body: JSON.stringify({
                name: name.value, surname: surname.value, rule: rule,
                contact: phone.value, password: password.value, email: email.value
              })
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            if(res.errors)
            {
                
                if(res.errors.email){ Alert.alert(res.errors.email[0]) }
                if(res.errors.password){ Alert.alert(res.errors.password[0]) }
               
            }
            else{
                Alert.alert(res.message)
            }
        })
        .catch((error) => { console.warn(error);  })
  }

    return (

        <View style={styles.container}>

           <View style={styles.header}>
                <HeaderNavigationBar name="Ajouter un patient"  navigation={navigation} />
            </View>

            <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={styles.content} >

                <TextInput
                    label="Nom"
                    returnKeyType="next"
                    value={name.value}
                    onChangeText={text => setName({ value: text, error: '' })}
                    error={!!name.error}
                    errorText={name.error}
                />

               <TextInput
                    label="Prenom"
                    returnKeyType="next"
                    value={surname.value}
                    onChangeText={text => setSurname({ value: text, error: '' })}
                    error={!!surname.error}
                    errorText={surname.error}
                />

                <TextInput
                    label="Email"
                    returnKeyType="next"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text, error: '' })}
                    error={!!email.error}
                    errorText={email.error}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                />

                <TextInput
                    label="Telephone"
                    returnKeyType="next"
                    value={phone.value}
                    onChangeText={text => setPhone({ value: text, error: '' })}
                    error={!!phone.error}
                    errorText={phone.error}
                    keyboardType="numeric"
                />

                <TextInput
                    label="Mot de passe"
                    returnKeyType="done"
                    value={password.value}
                    onChangeText={text => setPassword({ value: text, error: '' })}
                    error={!!password.error}
                    errorText={password.error}
                    secureTextEntry
               />

                { !loading  ? 
                    <Button mode="contained" onPress={_onSave} >
                           Enregistrer
                    </Button> 
                    :
                    <ActivityIndicator animating={true} color='#60bdb2' />
                }
                


            </KeyboardAvoidingView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:3,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
   
  });
