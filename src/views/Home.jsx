import React, {useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Alert} from 'react-native';
import { useSelector } from "react-redux";
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { Surface,Card } from 'react-native-paper';

export default function Home() {

    const navigation = useNavigation()
    const currentUser = useSelector((state) => state)

    const navigateToForfait = () => {
        if(currentUser.toggleConnexion.forfait_id) Alert.alert('Vous avez deja un forfait en cours!!')
        else navigation.navigate('Forfait')
    }

    const MyComponent = () => {

        if(currentUser.toggleConnexion.role === "patient")
        {
            return <View style={styles.content}>

             
            <View style={{ flex:3, flexDirection:'row', marginTop: 60 }} >

                <View style={{ flex:1 }} >
                    <TouchableOpacity onPress = { () => navigation.navigate('Profil')}>
                       <Image source={require('./../../assets/signin.png')} style={styles.image} />
                    </TouchableOpacity>
                </View>

                <View style={{ flex:1 }} >
                    <TouchableOpacity onPress = { () => navigation.navigate('Password')}>
                       <Image source={require('./../../assets/password.png')} style={styles.image} />
                    </TouchableOpacity>
                </View>
               
            </View>

            <View style={{flexDirection:'row'}} >
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text>Mon compte</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text>Mot de passe</Text>
                    </View>
            </View>

            <View style={{ flex:3, flexDirection:'row', marginTop:20 }} >

                <View style={{ flex:1 }} >
                    <TouchableOpacity onPress={ () => navigation.navigate('DossierMedical')}>
                       <Image source={require('./../../assets/medical_1.png')} style={styles.image} />
                    </TouchableOpacity>
                </View>

                <View style={{ flex:1 }} >
                    <TouchableOpacity onPress = { navigateToForfait }>
                       <Image source={require('./../../assets/forfait.png')} style={styles.image} />
                    </TouchableOpacity>
                </View>
               
            </View>
            <View style={{ flexDirection:'row'}} >
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text>Dossier medical</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text>Forfait</Text>
                    </View>
            </View>

            <View style={{ flex:1, flexDirection:'row' }} >
               
            </View>
            
        </View>
        }

        if(currentUser.toggleConnexion.role === "infirmier")
        {
            return <View style={styles.content}>

                        <View style={{flex:1}}>

                           <Image source={require('./../../assets/nurse.png')} style={styles.image} />

                        </View>

                        <View style={{flex:3, marginTop:35}}>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                               onPress={ () => { navigation.navigate('RdvList')}}
                                    >
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>Liste des RDV</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                           onPress={ () => { navigation.navigate('LastRdv')}}
                                    >
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>RDV récents</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                        onPress={ () => { navigation.navigate('Patient')}}
                                    >
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>Ajouter un patient</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} onPress={ () => navigation.navigate('ListPatient')}>
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>Liste des Patients</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>


                        </View>

                   </View>
        }

        if(currentUser.toggleConnexion.role === "medecin")
        {
            return <View style={styles.content}>

                        <View style={{flex:1}}>

                           <Image source={require('./../../assets/rdv.png')} style={styles.image} />

                        </View>

                        <View style={{flex:3, marginTop:35}}>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                               onPress={ () => { navigation.navigate('ListIntervention')}}
                                    >
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>Liste des interventions</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                           onPress={ () => { navigation.navigate('LastRdv')}}
                                    >
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>Interventions récentes</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>

                             <View style={{flex:1}}>
                               <Surface style={styles.surface}>

                                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} onPress={ () => navigation.navigate('ListPatient')}>
                                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                                        <Text style={styles.texte}>Liste des Patients</Text>
                                    </TouchableOpacity>

                                </Surface>
                             </View>


                        </View>

                   </View>
        }
    }
    
    return (
          <View style={styles.container}>
             
            <View style={styles.header}>
                <HeaderNavigationBar name="Accueil"  navigation={navigation} />
            </View>

           <MyComponent />
              

          </View>
          
    )

}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    image: {
        height: 150,
        width: 150,
    },
    surface: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: -8,
        height: 60,
        width: 300,
        justifyContent: 'center',
        elevation: 4,
        borderRadius: 25,
        marginBottom:10
    },
    texte:{
        fontSize: 17,
        color: 'black',
    },
  });