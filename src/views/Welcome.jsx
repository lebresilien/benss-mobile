import React from 'react';
import { View, Text, StyleSheet, Image} from 'react-native';

export default function Welcome() {

    return (

        <View style={styles.container}>

            <View  style={styles.container_texte}>
                <Text style={styles.texte}>Welcome!</Text>
            </View>
            
            <View  style={styles.container_image} >
                <Image style={styles.image}
                    
                    source={require('./../../assets/favicon.png')}
                />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#60bdb2',
      alignItems:'center'
    },
    container_texte:{
        flex:1,
        paddingTop:70,
    },
    texte: {
        fontSize:23,
        fontWeight:'bold',
        color:'white',
        textTransform: 'uppercase'
    },
    container_image:{
        flex:2,
    },
  });