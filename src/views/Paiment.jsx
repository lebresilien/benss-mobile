import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Picker, Alert} from 'react-native';
import { useSelector } from "react-redux";
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import Button from '../components/Button'
import { ActivityIndicator } from 'react-native-paper';
import { CreditCardInput } from "react-native-credit-card-input";

export default function Paiment({route}) {

    const{id, price} = route.params
    const navigation = useNavigation() 
    const[cardNumber, setCardNumber] = useState('')
    const[expMonth, setExpoMonth] = useState('')
    const[expYear, setExpYear] = useState('')
    const[cvc, setCvc] = useState('')
    const[loading, setLoading] = useState(false)
    const currentUser = useSelector((state) => state) 
    const stripe_url = "https://api.stripe.com/v1/";
    const secret_key = "pk_test_51IfSncLTp36GjDPFgLBge2ImxIom0yGQwrrA2eFivF1u2bFFoY80aj3UYJBas3lQ7JOWGNogPdkEhZIohPlAAdGl00o5LvUPoz";

   const _onChange = (form) => {
       setCardNumber(form.values.number)
       setExpoMonth(form.values.expiry.split('/')[0])
       setExpYear(form.values.expiry.split('/')[1])
       setCvc(form.values.cvc)
   }


   function souscription(id, price, token)
   {
        fetch('https://benss.bensscameroun.com/api/v1/users/forfaits', {
            method: 'POST',
            headers:{
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            },
            body: JSON.stringify({id: id, token: token, price: price})
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            Alert.alert(res.message)
        })
   }

   const _onPaid = () => {
       
    setLoading(true)
    const card = {
        'card[number]': cardNumber,
        'card[exp_month]': expMonth,
        'card[exp_year]': expYear,
        'card[cvc]': cvc
    };

    return fetch(stripe_url + "tokens", {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization: "Bearer " + secret_key,
        },
        body: Object.keys(card).map(key => key + '=' + card[key]).join('&')
    })
    .then(response => response.json())
    .then(response => { 
        if(response.error) Alert.alert(response.error.message)
        else{
            souscription(id, price, response.id)
        }
     })
       
   }

    return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <HeaderNavigationBar name="Paiment forfait"  navigation={navigation} />
                </View>

                <View style={styles.content}>

                    <CreditCardInput onChange={_onChange} />

                    {!loading ? 
                        <View style={{marginTop: 20}}>
                            <Button  mode="contained"  onPress={_onPaid}>
                                Souscrire
                            </Button>
                        </View>
                        :
                        <View style={{marginTop: 20}}><ActivityIndicator animating={true} color='#60bdb2' /></View>
                    }
                    

                </View>


            </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        marginTop:15
    },
    
});