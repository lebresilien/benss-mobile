import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ScrollView} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { List, ActivityIndicator, Text } from 'react-native-paper';
import { useSelector} from "react-redux";

export default function Biometry({ route }) {

    const navigation = useNavigation()
    const {id} = route.params
    const[biometries, setBiometries] = useState([])
    const[loading, setLoading] = useState(false)
    const currentUser = useSelector((state) => state)


    useEffect(() => {
       loadData()
    },[route.params.id])

    const loadData = async() => {

        setLoading(true)
        await fetch('https://benss.bensscameroun.com/api/v1/users/biometry/'+id, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            }
        })
        .then(res=>res.json())
        .then(res => {
            console.log(res.taille)
            setLoading(false)
            setBiometries(res.data)
        })
        .catch((error) => { console.warn(error);  })
    }

    return (

        <View style={styles.container}>

             <View style={styles.header}>
                <HeaderNavigationBar name="Biometrie"  navigation={navigation}/>
            </View>

            <View style={styles.content}>
                <ScrollView style={{flex:1, paddingLeft: 50, paddingRight: 50}}>
                    { !loading ? 
                        <View style={{flex:1}}>
                            {biometries ? 
                                <List.Section>
                                    <List.Item title="Taille" description={biometries.taille} />
                                    <List.Item title="Poids" description={biometries.poids} />
                                    <List.Item title="Temperature" description={biometries.temperature} />
                                    
                                </List.Section>:
                                <Text style={styles.texte}>Aucune information disponible</Text>
                            }
                        </View>
                        :
                        <ActivityIndicator animating={true} color='#60bdb2' />
                    }
                </ScrollView>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:3,
        backgroundColor: 'white'
    },
    texte: {
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 250,
        marginLeft: 20
    }
});