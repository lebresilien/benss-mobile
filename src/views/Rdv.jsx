import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Button from '../components/Button';
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { Surface } from 'react-native-paper'

export default function Rdv() {
    const navigation = useNavigation()
    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="rdv"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

                <View style={styles.image_block}>
                    <Image source={require('./../../assets/rdv.png')} style={{height:150, width:350}} />
                </View>


                <View style={styles.block_element}>

                    <Surface style={styles.surface}>

                        <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}}  onPress = { () => navigation.navigate('ListRdv')} >
                            <View style={{ }}>
                                <Image source={require('./../../assets/dossier.png')} style={{height:100, width:100, justifyContent: 'flex-start'}} />
                            </View>
                            <View  style={{alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={styles.texte}>Liste des RDV</Text>
                            </View>
                        
                        </TouchableOpacity>

                    </Surface>

                    <Surface style={styles.surface}>

                        <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} onPress = { () => navigation.navigate('RdvDate')} >
                            <View style={{ }}>
                                <Image source={require('./../../assets/dossier.png')} style={{height:100, width:100, justifyContent: 'flex-start'}} />
                            </View>
                            <View  style={{alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={styles.texte}>Prendre un RDV</Text>
                            </View>
                        
                        </TouchableOpacity>

                    </Surface>

                    <Surface style={styles.surface}>

                        <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}}  onPress = { () => navigation.navigate('AddRdv')} >
                            <View style={{ }}>
                                <Image source={require('./../../assets/dossier.png')} style={{height:100, width:100, justifyContent: 'flex-start'}} />
                            </View>
                            <View  style={{alignItems: 'center', justifyContent: 'center' }} >
                               <Text style={styles.texte}>Autres RDV</Text>
                            </View>
                        
                        </TouchableOpacity>

                    </Surface>

                </View>

            </View>

        </View>

    ) 
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
        
    },
    image_block: {
        flex:1,
        marginTop:10,
    },
    image: {
        height: 200,
        width: null
    },
    block_element: {
        flex:2,  
        marginTop: 35 
    },
    surface: {
        paddingTop: 2,
        paddingBottom: 2,
        height: 60,
        width: 300,
        justifyContent: 'center',
        elevation: 4,
        borderRadius: 25,
        marginBottom:10
    },
    texte:{
        fontSize: 17,
        color: 'black',
    },
  
});