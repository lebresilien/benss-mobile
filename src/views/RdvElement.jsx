import React, {useState, useEffect, useRef} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Picker, Alert} from 'react-native';
import Button from '../components/Button';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from "react-redux";
import { ActivityIndicator } from 'react-native-paper';

export default function RdvElement({route}) {

    const navigation = useNavigation()
    const currentUser = useSelector((state) => state)
    const[loading, setLoading] = useState(false)
    const[load, setLoad] = useState(false)
    const[services, setServices] = useState([])
    const[nurses, setNurses] = useState([])
    const[selectedService, setSelectedService] = useState('')
    const[selectedNurse, setSelectedNurse] = useState('')
    const { time, date } = route.params;

     useEffect(() => {
        getUserData()
    }) 

    const _onSaveRdv = async() => {
        setLoading(true)
        await fetch('https://benss.bensscameroun.com/api/v1/rdvs', {
            method: 'POST',
            headers:{
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            },
            body: JSON.stringify({nurse: selectedNurse, service_id: selectedService, date_rdv: date,heure_rdv: time})
          })
          .then(res=>res.json())
          .then(res => {
              setLoading(false)
              Alert.alert(res.message)
          })
          .catch((error) => { console.warn(error);  })
    }

    /* recuperation des services d'un patients */

    const getUserData = async() => {
      
      await fetch('https://benss.bensscameroun.com/api/v1/rdvs/users/data', {
      method: 'GET',
      headers:{
          'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
      },
    })
    .then(res=>res.json())
    .then(res => {
       setLoad(true)
       setServices(res.services)
       setNurses(res.nurses)
    })
    .catch((error) => { console.warn(error);  })

    }


    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="Prendre un rdv"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

                <TouchableOpacity style={{height:50, flexDirection: 'row', width: '80%', alignItems:'center'}}  >
                    <Text style={{flex:1, fontWeight: 'bold', fontSize: 15}}>Selectionnez un service</Text>
                    { load && (
                    <Picker mode="dropdown" 
                            selectedValue={selectedService}
                            style={styles.picker}
                            itemStyle={styles.pickerItem}
                            onValueChange={(itemValue, itemIndex) => setSelectedService(itemValue)}
                        >
                            { services.map((service, index) => (
                                    <Picker.Item label={service.name} value={service.id} key={index} />    
                            ))}
                    </Picker>
                    )}
                </TouchableOpacity>

                <TouchableOpacity style={{height:50, flexDirection: 'row', width: '80%', alignItems:'center'}}  >
                    <Text style={{flex:1, fontWeight: 'bold', fontSize: 15}}>Selectionnez un infirmier</Text>
                    { load && (
                    <Picker mode="dropdown" 
                            selectedValue={selectedNurse}
                            style={styles.picker}
                            itemStyle={styles.pickerItem}
                            onValueChange={(itemValue, itemIndex) => setSelectedNurse(itemValue)}
                        >

                            { nurses.map((nurse, index) => (
                                    <Picker.Item label={nurse.name} value={nurse.id} key={index} />    
                            ))}
                            
                            
                    </Picker>
                    )}
                </TouchableOpacity>

                { !loading  ?
                <Button mode="contained" onPress={_onSaveRdv} style={{width: '80%'}}>
                  Enregistrer
                </Button>
                :
                <ActivityIndicator animating={true} color='#60bdb2' />
              }
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
        
    },
    picker: {
        flex:1,
    },
    pickerItem: {
        height: 44
    }
});