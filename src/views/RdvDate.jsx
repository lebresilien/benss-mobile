import React, {useState, useEffect, useRef} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Picker, Alert} from 'react-native';
import Button from '../components/Button';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import { Icon } from 'native-base';
import { FontAwesome5 } from '@expo/vector-icons'
//import DatePicker from 'react-native-modern-datepicker'
import DatePicker from '@dietime/react-native-date-picker';

export default function RdvDate() {

    const navigation = useNavigation()
    const currentUser = useSelector((state) => state)
    const dispatch = useDispatch()
    const[image, setImage] = useState({image: require('./../../assets/rdv.png')})
    
    
    const [date, setDate] = useState();
    const[showDate, setShowdate] = useState(false)
    const dateA = new Date()

    const _onNavigate = () => {
        const newDate  = date.getFullYear() + '-'+ parseInt(date.getMonth() + 1 ) + '-' + date.getDate()
        /* const rdv = {date: newDate}
        const action = { type: "ADD-DATE", value: rdv }
        dispatch(action) */ 
        navigation.navigate('RdvTime', { date: newDate})
        
    }

   

    return (
        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="Prendre un rdv"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

            
            <TouchableOpacity style={{height:50, flexDirection: 'row', width: '80%', alignItems:'center'}}  >
               <Text style={{fontSize:25, fontWeight:'bold'}}>{date ? date.toDateString() : "Selectionnez une date"}</Text>
            </TouchableOpacity>

            <DatePicker
                value={date}
                onChange={(value) => setDate(value)}
                format="yyyy-mm-dd"
                startYear={dateA.getFullYear() - 1}
                endYear={ dateA.getFullYear() + 1}
            /> 
            
            <View style={{alignItems: 'flex-end'}}>
                <Button mode="contained" disabled={date ? false : true} onPress={ _onNavigate}>
                  Suivant
                </Button>
            </View>
                


            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
        
    },
    
});