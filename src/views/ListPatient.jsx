import React, {useEffect, useState} from 'react';
import { View, Text, StyleSheet, Image,TouchableOpacity,Platform, Alert, FlatList, SafeAreaView} from 'react-native';
import Button from '../components/Button'
import TextInput from '../components/TextInput';
import { theme } from '../core/theme';
import { ActivityIndicator } from 'react-native-paper';
import { useSelector} from "react-redux";
import { useNavigation } from '@react-navigation/native';
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function ListPatient() {

    const navigation = useNavigation()
    const[users, setUsers] = useState([])
    const[loading, setLoading] = useState(false)
    const currentUser = useSelector((state) => state)
    const[index, setIndex] = useState(0)
    const[limit, setLimit] = useState(1)
    let url = ''
    

    const getUsers = async() => {
        if(index <= limit ){
            setLoading(true)
            if(currentUser.toggleConnexion.role === "infirmier"){
                 url = 'https://benss.bensscameroun.com/api/v1/users/nurses/patients/'+index
            } 
            else{
                 url = 'https://benss.bensscameroun.com/api/v1/users/doctors/patients/'+index
            } 
            
            await fetch(url, {
                method: 'GET',
                headers:{
                    'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
                }
            })
            .then(res=>res.json())
            .then(res => {
                setLoading(false)
                setUsers(users.concat(res.users))
                setIndex(res.index)
                setLimit(res.limit)
            })
            .catch((error) => { console.warn(error);  })
       }
    }

    

    const Item = ({name, max, id}) => (

        <View style={styles.item}>
            <View style={{flex:4}}>
              <Text numberOfLines={1} style={styles.name}>
                    { ((name).length > max) ? 
                          (((name).substring(0,max-3)) + '...') : name 
                    }
              </Text>
            </View>
            <View style={{flex:1}} >
                <TouchableOpacity  onPress={ () => navigation.navigate('DetailsPatient', { id: id })}>
                   <Icon name="info-circle" size={30} color="#60bdb2" />
                </TouchableOpacity>
            </View>
            <View style={{flex:1, alignItems: 'flex-end'}}>
                <TouchableOpacity onPress={ () => navigation.navigate('Medical', { id: id })}>
                   <Icon name="address-book" size={30} color="#60bdb2" />
                </TouchableOpacity>
            </View>
        </View>
    )

    const renderItem = ({ item }) => (
        <Item name={ item.surname ? `${item.name} ${item.surname}` : item.name} id={item.id}  max={20} />
    )

    useEffect(() => {
      getUsers()
    })

    return(

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="Liste patients"  navigation={navigation} />
            </View>

            {!loading ? 
            <SafeAreaView style={styles.content}>
                
                  {users.length > 0 ?
                    <FlatList
                        data={users}
                        renderItem={renderItem}
                        keyExtractor={item => item.id.toString()}
                        onEndReachedThreshold={0.1}
                        onEndReached={getUsers}
                    />:
                    <Text></Text>
                  }
                   
                
            </SafeAreaView>:
            
            <View style={styles.content}><ActivityIndicator animating={true} color='#60bdb2' /></View>
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:3,
        paddingRight:20,
        paddingLeft:20,
        marginTop:20
    },
    item: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical:2
    },
    name :{
        fontSize:19, 
        fontWeight: 'bold',
    }
   
  });
