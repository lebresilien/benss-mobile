import React, {useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Alert} from 'react-native';
import { useSelector } from "react-redux";
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import Button from './../components/Button'
import TextInput from './../components/TextInput';
import { useNavigation } from '@react-navigation/native';
import { passwordValidator } from '../core/utils';
import { ActivityIndicator } from 'react-native-paper';


export default function Password() {

    const navigation = useNavigation() 
    const [confirmPassword, setConfirmPassword] = useState({ value: '', error: '' });
    const [newPassword, setNewPassword] = useState({ value: '', error: '' });
    const [password, setPassword] = useState({ value: '', error: '' });
    const[error, setError] = useState('')
    const[errorPassword, setErrorPassword] = useState('')
    const[errorConfirmPassword, setErrorConfirmPassword] = useState('')
    const[loading, setLoading] = useState(false)
    const currentUser = useSelector((state) => state)
    const[success, setSuccess] = useState('')

    const _onUpdatePassword = async() => {

        const passwordError = passwordValidator(password.value);
        const passwordNewError = passwordValidator(newPassword.value);
        const passwordConfirmError = passwordValidator(confirmPassword.value);

        if (passwordError || passwordNewError || passwordConfirmError) {
            setNewPassword({ ...passwordNewError, error: passwordNewError });
            setConfirmPassword({ ...confirmPassword, error: passwordConfirmError });
            setPassword({ ...password, error: passwordError });
            return;
        }

        setLoading(true)
        setError('')
        setErrorPassword('')
        setErrorConfirmPassword('')
        setSuccess('')

        await fetch('https://benss.bensscameroun.com/api/v1/users/password', {
            method: 'POST',
            headers:{
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${currentUser._token}`
            },
            body: JSON.stringify({motdepasse: password.value, password: newPassword.value, password_confirmation: confirmPassword.value})
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            if(res.errors)
            {
                if(res.errors.password) setErrorPassword(res.errors.password)
                if(res.errors.password_confirmation) setErrorConfirmPassword(res.errors.password_confirmation)
            }
            else
            {
                if(res.error) setError(res.error)
                if(res.success) {
                    setSuccess(res.success)
                    Alert.alert('Modification effectuée')
                }
            }
            
            
           
            
        })
        .catch((error) => { console.warn(error);  })
    };

    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="Modifier mot de passe"  navigation={navigation} />
            </View> 

            <View style={styles.content}>
    
                <View  >
                    { error ? <Text style={styles.error}>{error}</Text>   : <Text></Text>} 
                    { errorPassword ? errorPassword.map((el,index) => ( <Text key={index} style={styles.error}>{el}</Text> ))  : <Text></Text>}
                    { errorConfirmPassword ? errorConfirmPassword.map((el,index) => ( <Text key={index} style={styles.error}>{el}</Text> ))  : <Text></Text>}
                    { success ? <Text style={styles.success}> { success} </Text> :  <Text></Text>}
                </View>
                <TextInput
                    label="Entrer votre mot de passe"
                    returnKeyType="done"
                    value={password.value}
                    onChangeText={text => setPassword({ value: text, error: '' })}
                    error={!!password.error}
                    errorText={password.error}
                    secureTextEntry
                />

                <TextInput
                    label="Nouveau Mot de passe"
                    returnKeyType="done"
                    value={newPassword.value}
                    onChangeText={text => setNewPassword({ value: text, error: '' })}
                    error={!!newPassword.error}
                    errorText={newPassword.error}
                    secureTextEntry
                />

               <TextInput
                    label="Confirmez votre Mot de passe"
                    returnKeyType="done"
                    value={confirmPassword.value}
                    onChangeText={text => setConfirmPassword({ value: text, error: '' })}
                    error={!!confirmPassword.error}
                    errorText={confirmPassword.error}
                    secureTextEntry
                />

                { !loading  ?
                    <Button mode="contained" onPress = { _onUpdatePassword } >
                             Modifier
                    </Button>
                    :
                    <ActivityIndicator animating={true} color='#60bdb2' />
                }

            </View>

        </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2',
    },
    content: {
        flex:2,
        paddingLeft: 25,
        paddingRight: 25
    },
    error:{
        color: 'red',
        fontWeight:'bold',
    },
    success: {
        color: '#60bdb2',
        fontWeight:'bold',
    }
  
});