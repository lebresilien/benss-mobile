import React, {useState,useEffect} from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Alert} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { List, ActivityIndicator } from 'react-native-paper';
import { useSelector } from "react-redux";

export default function DetailsIntervention({ route }) {

    const navigation = useNavigation()
    const { id } = route.params;
    const currentUser = useSelector((state) => state)
    const[loading, setLoading] = useState(false)
    const[rdv, setRdv] = useState([])

    useEffect(() => {
        loadData()
     },[route.params.id])

     const loadData = async() =>  {

        setLoading(true)
        await fetch('https://benss.bensscameroun.com/api/v1/rdvs/'+id, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            }
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            setRdv(res) 
        })
        .catch((error) => { console.warn(error);  })
    }

    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="details intervention"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

                    <ScrollView style={{flex:2, paddingLeft: 20, paddingRight: 20}}>
                    {!loading ?
                        <View style={{flex:1}}>
                            {rdv.map((data, index) => (

                                <List.Section key={index}>
                                  <List.Item title="Noms et prénoms " description={data.user.surname ? `${data.user.name} ${data.user.surname}` : data.user.name } />
                                  <List.Item title="Service " description={data.service.name } />
                                  <List.Item title="Date et Heure" description={data.date_rdv} />
                                  <List.Item title="Infirmier"  description={data.nurse.surname ? `${data.nurse.name} ${data.nurse.surname}` : rdatadv.nurse.name } />
                                  <List.Item title="Medecin"  description={data.doctor.surname ? `${data.doctor.name} ${data.doctor.surname}` : data.doctor.name } />
                                  <List.Item title="Taille"  description={data.taille } />
                                  <List.Item title="Poids"  description={data.poids } />
                                  <List.Item title="Temperature"  description={data.temperature} />
                                  <List.Item title="Observations"  description={data.observation} />
                                  <List.Item title="Ordonnance"  description={data.ordonnance} />
                                </List.Section>
                                  
                            )) }
                            
                                    
                        </View>:
                       <ActivityIndicator animating={true} color='#60bdb2' />
                    }

                    </ScrollView>
                    
                 
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        backgroundColor: 'white',
    
    },
});