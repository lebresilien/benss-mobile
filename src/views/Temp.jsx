import React, {useState, useEffect} from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import SignIn from './SignIn';
import Home from './Home';
import { useSelector } from 'react-redux'

export default function Temp() {

    const currentUser = useSelector((state) => state) 

    return (

            <View style={styles.container}>
              { currentUser.toggleConnexion.connexion ? <Home /> : <SignIn /> }
           </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
});