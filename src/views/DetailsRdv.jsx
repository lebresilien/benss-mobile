import React from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Alert} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { List } from 'react-native-paper';
import { useSelector } from "react-redux";

export default function DetailsRdv({ route }) {

    const navigation = useNavigation()
    const { date, nurse_name, doctor_name, nurse_surname, doctor_surname, statut, service } = route.params;
    const currentUser = useSelector((state) => state)

    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="details rendez-vous"  navigation={navigation}/>
            </View>

            <View style={styles.content}>
            <List.Section>
                <List.Subheader style={{fontWeight: 'bold', fontSize: 25}}>{ service }</List.Subheader>
                <List.Item title={ doctor_surname ? `${doctor_name}  ${doctor_surname}` : doctor_name} description="Nom du medecin" />
                <List.Item title={ nurse_surname ? `${nurse_name}  ${nurse_surname}` : nurse_name} description="Nom de l'infirmier" />
                <List.Item title={date}  description="Date et heure du rendez-vous" />
                {currentUser.toggleConnexion.role === "infirmier" ? 
                  <List.Item title={statut ? 'Oui' : 'Non'}  description="Rendez-vous confirmé" />:
                  <List.Item title=''  description="" />
                }
            </List.Section>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
});