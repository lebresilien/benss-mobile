import React, {useState, useEffect, useRef} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Platform, Alert} from 'react-native';
import Button from '../components/Button';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import DatePicker from 'react-native-modern-datepicker';
import DateTimePicker from '@react-native-community/datetimepicker';

export default function RdvTime({route}) {

    const navigation = useNavigation()
    const[image, setImage] = useState({image: require('./../../assets/rdv.png')})
    const[time, setTime] = useState('');
    const [dat, setDate] = useState(new Date(1598051730000));
    const[show, setShow] = useState(false);
    const currentUser = useSelector((state) => state)
    const dispatch = useDispatch()
    const { date } = route.params;


   const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || dat;
    setShow(Platform.OS === 'ios');
    setTime(currentDate.getHours()+':'+currentDate.getMinutes());
  };

    return(

        <View style={styles.container}>

             <View style={styles.header}>
                <HeaderNavigationBar name="Prendre un rdv"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

                <TouchableOpacity style={{height:50, flexDirection: 'row', width: '80%', justifyContent:'center', alignItems: 'center'}} 
                                  onPress={ () => setShow(true) } 
                >
                    <Text style={{fontSize:25, fontWeight:'bold'}}>{time ? time : "Cliquez pour selectionner une Heure"}</Text>
                </TouchableOpacity>

                  {show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={dat}
                        mode="time"
                        is24Hour={true}
                        display="default"
                        onChange={onChange}
                    />
                  )}
                

                <View style={{width:'80%', alignItems:'center', marginTop:50}}>
                    <Button mode="contained" disabled={time ? false : true}
                            onPress={ () => navigation.navigate('RdvElement', {time:time, date:date})}
                     >
                        Suivant</Button>
                </View>
               

            </View>


        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
        
    }   
});