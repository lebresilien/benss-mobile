import React, {useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Alert} from 'react-native';
import Button from '../components/Button'
import TextInput from '../components/TextInput';
import { theme } from '../core/theme';
import { passwordValidator, emailValidator } from '../core/utils';
import { ActivityIndicator } from 'react-native-paper';
import { useSelector, useDispatch} from "react-redux";

export default function SignIn() {

  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });
  const[error, setError] = useState('')
  const[loading, setLoading] = useState(false)
  const dispatch = useDispatch()
  const currentUser = useSelector((state) => state) 

  const _onLoginPressed = async() => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    
    if(emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    setLoading(true)
    setError('')

    await fetch('https://benss.bensscameroun.com/api/v1/auth/login', {
      method: 'POST',
      headers:{
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({email: email.value, password: password.value})
    })
    .then(res=>res.json())
    .then(res => {
        setLoading(false)
        if(res.message)  setError(res.message)

        if(res.success)
        {
           const user = {
                          id:res.id,role:res.role,name:res.name,email:res.email,_token:res.access_token,
                          surname:res.surname,phone:res.phone,user_id:res.user_id,forfait_id:res.forfait_id,
                          lieu:res.lieu,situation:res.situation,profession:res.profession,
                        }
           const action = { type: "CONNEXION", value: user }
           dispatch(action)
        }
    })
    .catch((error) => { console.warn(error);  })
  };

    return (

        <View style={styles.container} >

            <View style={styles.container_description} >

                <View style={styles.text_connexion}>
                    
                     <Text style={styles.texte}>Connexion</Text>

                </View>

                <View style={styles.container_image}>

                    <Image style={styles.image}
                        
                        source={require('./../../assets/signin.png')}
                    />
                </View>

                <View style={{flex: 1}}></View><View style={{flex: 1}}></View>

            </View>

            <View style={styles.container_form} >
               { error ? <Text style={styles.error}> {error} </Text> : <Text></Text>}
              <TextInput
                label="Email"
                returnKeyType="next"
                value={email.value}
                onChangeText={text => setEmail({ value: text, error: '' })}
                error={!!email.error}
                errorText={email.error}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
              />

              <TextInput
                label="Password"
                returnKeyType="done"
                value={password.value}
                onChangeText={text => setPassword({ value: text, error: '' })}
                error={!!password.error}
                errorText={password.error}
                secureTextEntry
              />

              {/* <View style={styles.forgotPassword}>
                <TouchableOpacity>
                  <Text style={styles.labelForgot}>Mot de passe oublié?</Text>
                </TouchableOpacity>
              </View> */}

              { !loading  ?
                <Button mode="contained" onPress={_onLoginPressed} >
                  Connexion
                </Button>
                :
                <ActivityIndicator animating={true} color='#60bdb2' />
              }
                
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    text_connexion :{
      flex: 1
    },
    container_image :{
      flex: 2,

    },
    container_description: {
           flex: 1,
           justifyContent: 'space-around',
           alignItems:'center',
           backgroundColor: '#60bdb2',
           paddingTop:60,           
    },
    texte: {
        fontSize:17,
        fontWeight:'bold',
        textTransform: 'uppercase',
        color: 'white'
    },
    image: {
       width: 200,
       height: 200
    },
    container_form: {
        flex: 2,
        backgroundColor: 'white',
        justifyContent:'center',
        paddingRight:25,
        paddingLeft:25
    },
    forgotPassword: {
      width: '100%',
      alignItems: 'flex-end',
      marginBottom: 24,
    },
    label: {
      color: theme.colors.secondary,
    },
    labelForgot: {
      color: "#60bdb2",
    },
    error:{
      color: 'red',
      fontWeight:'bold',
    }
  });