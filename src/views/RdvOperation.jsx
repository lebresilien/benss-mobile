import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { List, ActivityIndicator } from 'react-native-paper';
import { useSelector} from "react-redux";
import Button from '../components/Button'
import Modal from 'react-native-modal';
import TextInput from '../components/TextInput';
import { tempValidator, poidsValidator, tailleValidator, ordonnanceValidator, observationValidator } from '../core/utils';

export default function RdvOperation({ route }) {

    const navigation = useNavigation()
    const {id} = route.params
    const[user, setUser] = useState([])
    const[loading, setLoading] = useState(false)
    const[loaded, setLoaded] = useState(false)
    const[load, setLoad] = useState(false)
    const[state, setState] = useState(true)
    const[consult, setConsult] = useState(true)
    const currentUser = useSelector((state) => state)
    const [next, setNext] = useState(false);
    const [isModalVisible, setModalVisible] = useState(false);
    const [temp, setTemp] = useState({ value: '', error: '' });
    const [taille, setTaille] = useState({ value: '', error: '' });
    const [poids, setPoids] = useState({ value: '', error: '' });
    const [ordonnance, setOrdonnance] = useState({ value: '', error: '' });
    const [observation, setObservation] = useState({ value: '', error: '' });
    
    const showModal = () => setModalVisible(true);
    const hideModal = () => setModalVisible(false);
    const toggleNext = () => {
        const tailleError = tailleValidator(taille.value);
        const poidsError = poidsValidator(poids.value);
        const tempError = tempValidator(temp.value);

        if(tailleError || poidsError || tempError ) {
            setTaille({ ...taille, error: tailleError });
            setPoids({ ...poids, error: poidsError });
            setTemp({ ...temp, error: tempError });
           
            return;
        }
        setNext(!next);
    }; 


    useEffect(() => {
       loadData()
    },[route.params.id])

    function loadData() {

        setLoading(true)
        fetch('https://benss.bensscameroun.com/api/v1/rdvs/'+id, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            }
        })
        .then(res=>res.json())
        .then(res => {
            setUser(res)
            if(res[0].state === "1") setState(true) 
            else setState(false)

            if(res[0].consult === "1") setConsult(true)
            else setConsult(false)
            
            setLoading(false)
        })
        .catch((error) => { console.warn(error);  })
    }

    const _onConfirm = () => {
        setLoad(true)
        fetch('https://benss.bensscameroun.com/api/v1/rdvs/confirm/'+id, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            }
        })
        .then(res=>res.json())
        .then(res => {
            setLoad(false)
            setState(true)
            Alert.alert(res.message)
        })
        .catch((error) => { console.warn(error);  })
    }

    const _onConsult = async() => {
       
        const ordonnanceError = ordonnanceValidator(ordonnance.value);
        const observationError = observationValidator(observation.value);
        
        if(ordonnanceError || observationError ) {
         
          setOrdonnance({ ...ordonnance, error: ordonnanceError });
          setObservation({ ...observation, error: observationError });
          return;
        }
        setLoaded(true)
        await fetch('https://benss.bensscameroun.com/api/v1/rdvs/doctor/soins', {
        method: 'POST',
        headers:{
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
        },
        body: JSON.stringify({
                              poids: poids.value, taille: taille.value, temp: temp.value,key:route.params.id, 
                              ordonnance: ordonnance.value, observation: observation.value
                            })
        })
        .then(res=>res.json())
        .then(res => {
            setLoaded(true)
            setConsult(true)
            hideModal()
            Alert.alert(res.message)
        })
        .catch((error) => { console.warn(error);  })
        }

    return (

        <View style={styles.container}>

             <View style={styles.header}>
                <HeaderNavigationBar name="details rdv"  navigation={navigation}/>
            </View>

            <View style={styles.content}>
                <ScrollView style={{flex:2, paddingLeft: 20, paddingRight: 20}}>
                    <View style={{flex:3}}>
                        { !loading ? 
                           <View>
                               {user ?
                                 <View>
                                { user.map((item,index) => (

                                
                                <List.Section key={index}>
                                    <List.Item title="Noms et prénoms " description={item.user.surname ? `${item.user.name} ${item.user.surname}` : item.user.name } />
                                    <List.Item title="Service " description={item.service.name } />
                                    <List.Item title="Date et Heure" description={item.date_rdv} />
                                    {currentUser.toggleConnexion.role != "infirmier" ? 
                                        <List.Item title="Infirmier"  description={item.nurse.surname ? `${item.nurse.name} ${item.nurse.surname}` : item.nurse.name } />:
                                        <List.Item title="Medecin"  description={item.doctor.surname ? `${item.doctor.name} ${item.doctor.surname}` : item.doctor.name } />
                                    }
                                    
                                </List.Section>
                                ))}
                                </View>
                                :
                                <Text sytle={{textAlign:'center', marginTop:125, fontWeight: 'bold'}}>
                                    Aucune Information
                                </Text>
                               }
                            </View>
                            :
                            <ActivityIndicator animating={true} color='#60bdb2' />
                        }
                    </View>

                    <View style={{flex:1}}>
                     
                      {!state ? 
                       
                       <View style={{flex:1}}>

                            {!load ? 
                                <Button mode="contained" onPress={_onConfirm}>
                                        Confirmer
                                </Button> :
                                <ActivityIndicator animating={true} color='#60bdb2' /> 
                            } 
                            
                        </View>:
                        <Text></Text>

            
                      }

                        {currentUser.toggleConnexion.role === "infirmier" ? 

                             <Text></Text>:

                             <View style={{flex:1}}>
                                 {!consult ?  
                                    <Button mode="contained" onPress={showModal}>
                                        Intervention
                                    </Button>:
                                    <Text></Text>
                                 }
                             </View>
                         }

                      

                    </View>
                    <View style={styles.modal}>
                        <Modal isVisible={isModalVisible}>

                            {!next ?
                                <KeyboardAwareScrollView behavior="padding" 
                                                    resetScrollToCoords={{ x: 0, y: 0 }}
                                                    style={styles.modal_container}
                                                    scrollEnabled={false}
                                >


                                    <TextInput
                                        label="Temperature"
                                        returnKeyType="next"
                                        value={temp.value}
                                        onChangeText={text => setTemp({ value: text, error: '' })}
                                        error={!!temp.error}
                                        errorText={temp.error}
                                        keyboardType="numeric"
                                    />

                                    <TextInput
                                        label="Poids"
                                        returnKeyType="next"
                                        value={poids.value}
                                        onChangeText={text => setPoids({ value: text, error: '' })}
                                        error={!!poids.error}
                                        errorText={poids.error}
                                        keyboardType="numeric"
                                    />

                                    <TextInput
                                        label="Taille"
                                        returnKeyType="next"
                                        value={taille.value}
                                        onChangeText={text => setTaille({ value: text, error: '' })}
                                        error={!!taille.error}
                                        errorText={taille.error}
                                        keyboardType="numeric"
                                    />

                                    
                            
                                    <View style={{flexDirection:'row'}}>
                                        <Button  onPress={hideModal} style={{alignItems:'flex-start', width:120, marginRight:10}}>
                                            annuler
                                        </Button>
                                        <Button  onPress={toggleNext} style={{alignItems:'flex-end', width:120}}>
                                            suivant
                                        </Button>
                                    </View>
                                    

                                </KeyboardAwareScrollView>:

                                <KeyboardAwareScrollView behavior="padding" 
                                resetScrollToCoords={{ x: 0, y: 0 }}
                                style={styles.modal_container}
                                scrollEnabled={false}
                                >
                                    <TextInput
                                        label="Observations"
                                        returnKeyType="next"
                                        value={observation.value}
                                        onChangeText={text => setObservation({ value: text, error: '' })}
                                        error={!!observation.error}
                                        errorText={observation.error}
                                        
                                    />

                                    <TextInput
                                        label="Ordonnance"
                                        returnKeyType="next"
                                        value={ordonnance.value}
                                        onChangeText={text => setOrdonnance({ value: text, error: '' })}
                                        error={!!ordonnance.error}
                                        errorText={ordonnance.error}
                                        
                                    />  

                                    

                                    {!loaded ? 
                                        <View style={{flexDirection:'row'}}>
                                            <Button  onPress={toggleNext} style={{alignItems:'flex-start', width:120, marginRight:10}}>
                                                retour
                                            </Button>
                                            <Button  onPress={_onConsult} style={{alignItems:'flex-end', width:120}}>
                                                Enregistrer
                                            </Button>
                                        </View>:
                                        <ActivityIndicator animating={true} color='#60bdb2' />
                                    }
                                </KeyboardAwareScrollView>
                            }

                        </Modal>
                    </View>

                    



                </ScrollView>
            </View>


        </View>
    )

}
const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:3,
        backgroundColor: 'white'
    },
    modal: {
       flex:1
    },
    modal_container: {
        flex:1,
        backgroundColor: 'white',
        borderRadius: 25,
        padding: 35,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        height:300
    }
});