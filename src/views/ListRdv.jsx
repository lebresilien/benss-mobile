import React, {useState, useEffect,} from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Alert} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from "react-redux";
import { ActivityIndicator } from 'react-native-paper';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon } from 'native-base';


export default function ListRdv() {

    const navigation = useNavigation()
    const currentUser = useSelector((state) => state)
    const[loading, setLoading] = useState(true)
    const[rdvs, setRdvs] = useState([]) 

    useEffect(() => {
        getAllRdv()
    }) 

    const getAllRdv = async() => {
        await fetch('https://benss.bensscameroun.com/api/v1/rdvs/patient', {
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            },
          })
          .then(res=>res.json())
          .then(res => {
              setRdvs(res.data)
              setLoading(false)
          })
          .catch((error) => { console.warn(error);  })
    }


    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="liste rendez-vous"  navigation={navigation}/>
            </View>

            <View style={styles.content}>

                <SafeAreaView style={styles.SafeAreaView}>
                   <ScrollView >

                       { loading ? 
                            <ActivityIndicator animating={true} color='#60bdb2' /> :

                            <View>
                               {rdvs.length == 0 ? 
                                    <Text style={{textAlign:'center', marginTop:35}}></Text>:
                                    <List>
                                        {rdvs.map((rdv, index) => (

                                                <ListItem key={index} onPress={ () => navigation.navigate('DetailsRdv', {
                                                                                                date:rdv.date_rdv, nurse_name:rdv.nurse.name, nurse_surname:rdv.nurse.surname, service:rdv.service.name,
                                                                                                statut:rdv.state,doctor_name:rdv.doctor.name, doctor_surname:rdv.doctor_surname
                                                                                            })}>
                                                    <Left>
                                                        <Text style={{fontWeight: 'bold'}}>{ rdv.service.name }</Text>
                                                    </Left>
                                                    <Right>
                                                        <Icon name="arrow-forward" />
                                                    </Right>
                                                </ListItem>
                                        ))}
                                    </List>
                               }
                            </View>
                       }

                   </ScrollView>
                </SafeAreaView>

            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        backgroundColor: 'white'
    },
    SafeAreaView: {
        flex:1,
       justifyContent: 'center'
    }
});