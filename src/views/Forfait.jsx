import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Picker, Alert} from 'react-native';
import { useSelector } from "react-redux";
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import Button from '../components/Button'
import { TextInput } from 'react-native-paper';
import stripe from 'react-native-stripe-payments';
import { ActivityIndicator } from 'react-native-paper';



export default function Forfait() {

    const navigation = useNavigation() 
    const[forfaits, setForfaits] = useState([])
    const[selectedForfait, setSelectedForfait] = useState('')
    const[description, setDescription] = useState('')
    const[price, setPrice] = useState('')
    const[visite, setVisite] = useState('')
    const[loading, setLoading] = useState(false)
    //const[search, setSearch] = useState(false)
    const currentUser = useSelector((state) => state)
    


    const _onSelectedFofait = async(value, index) => {
        
        setLoading(true)
        setSelectedForfait(value)
        await fetch('https://benss.bensscameroun.com/api/v1/users/forfaits/'+value, {
        method: 'GET',
        headers:{
            'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
        },
      })
      .then(res=>res.json())
      .then(res => {
         setLoading(false)
         setPrice(res.forfait.price)
         setVisite(res.forfait.nbre_visite)
         setDescription(res.forfait.description)
      })
      .catch((error) => { console.warn(error);  })
        
    }

    useEffect(() => {
        getAllForfait()
    })

    const getAllForfait = async() => {

       await fetch('https://benss.bensscameroun.com/api/v1/users/forfaits', {
        method: 'GET',
        headers:{
            'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
        },
      })
      .then(res=>res.json())
      .then(res => {
         setForfaits(res.forfaits)
      })
      .catch((error) => { console.warn(error);  })
  
      } 
    

    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="souscrire à un Forfait"  navigation={navigation} />
            </View> 

            <View style={styles.content}>

                <View style={{marginTop: 50, width:'90%', alignItems: 'center'}}>

                    <TouchableOpacity style={{height:50, flexDirection: 'row', width: '80%', alignItems:'center'}}  >
                            <Text style={{flex:1, fontWeight: 'bold', fontSize: 17}}>Selectionnez un forfait</Text>
                            <Picker mode="dropdown" 
                                    selectedValue={selectedForfait}
                                    style={styles.picker}
                                    itemStyle={styles.pickerItem}
                                    onValueChange={(value, index) => _onSelectedFofait(value, index)}
                            >
                                {forfaits.map((forfait, index) => (
                                        <Picker.Item label={forfait.name} value={forfait.id} key={index} />    
                                ))} 
                                
                            </Picker>
                    </TouchableOpacity>

                </View>

                { !loading ? 
                    <View style={{marginTop: 50, width:'90%', alignItems: 'center'}}>

                        <View >
                            <Text style={styles.texte}>Prix du forfait : { price } </Text>
                        </View>

                        <View >
                            <Text style={styles.texte}>Nombre de visite/mois : { visite } </Text>
                        </View>

                        <View >
                           <Text style={styles.texte}>Description : { description } </Text>
                        </View>

                    </View> :

                    <View style={{marginTop: 20}}><ActivityIndicator animating={true} color='#60bdb2' /></View>
                }


                
                <Button mode="contained"  onPress={ () => navigation.navigate('Paiment',{id:selectedForfait, price: price})}>
                  Suivant
                </Button>
             

            </View>

        </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
    },
    picker: {
        flex:1,
    },
    pickerItem: {
        height: 44
    },
    texte: {
        fontSize:20,
        fontWeight: 'bold'
    }
  
});