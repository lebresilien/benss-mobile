import React, { useEffect, useState } from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Alert} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { ActivityIndicator } from 'react-native-paper';
import { useSelector} from "react-redux";
import { Container, Header, Body, List, ListItem, Text, Left, Right, Icon } from 'native-base'

export default function DetailsPatient({ route }) {

    const navigation = useNavigation()
    const[user, setUser] = useState([])
    const[loading, setLoading] = useState(false)
    const currentUser = useSelector((state) => state)


    useEffect(() => {
       loadData()
    })

    const loadData = () => {

        setLoading(true)
        fetch('https://benss.bensscameroun.com/api/v1/rdvs/nurse', {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            }
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            setUser(res.data)
        })
        .catch((error) => { console.warn(error);  })
    }

    return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <HeaderNavigationBar name="Liste Rdvs"  navigation={navigation}/>
                </View>

                <View style={styles.content}>

                    <ScrollView style={{flex:1}}>

                       {user.length == 0 ? 
                         <Text>Aucun Rdv</Text>:

                         <List>
                             {user.map((user, index) => (

                                 <ListItem onPress={() => navigation.navigate('RdvOperation',{id: user.id})} key={index}>
                                     <Left>
                                        <Text>
                                            { user.user.surname ? `${user.user.name} ${user.user.surname}` : user.user.name  }
                                        </Text>
                                     </Left>
                                     <Body>
                                         <Text>{user.service.name}</Text>
                                     </Body>
                                     <Right>
                                        <Icon name="arrow-forward" />
                                     </Right>
                                 </ListItem>
                             ))}
                         </List>
                       }
                    </ScrollView>
                  
                </View>

            </View>
    )

}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:3,
        backgroundColor: 'white'
    },
});