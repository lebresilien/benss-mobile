import React, {useState} from 'react';
import { View, Text, StyleSheet, Image, Picker, Alert, SafeAreaView,ScrollView } from 'react-native';
import { useSelector, useDispatch } from "react-redux";
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import Button from '../components/Button'
import TextInput from '../components/TextInput';
import DatePicker from 'react-native-modern-datepicker';
import { nameValidator, phoneValidator } from '../core/utils';
import { ActivityIndicator } from 'react-native-paper';

export default function Information() {

    const navigation = useNavigation()
    const currentUser = useSelector((state) => state)
    const dispatch = useDispatch()
    const [name, setName] = useState({ value: currentUser.toggleConnexion.name, error: '' });
    const [surname, setSurname] = useState({ value: currentUser.toggleConnexion.surname, error: '' });
    const [phone, setPhone] = useState({ value: currentUser.toggleConnexion.phone, error: '' });
    const [profession, setProfession] = useState({ value: currentUser.toggleConnexion.profession, error: '' });
    const [situation, setSituation] = useState({ value: currentUser.toggleConnexion.situation, error: '' });
    const[loading, setLoading] = useState(false)
    const [selectedSexe, setSelectedSexe] = useState(currentUser.toggleConnexion.sexe);
    //const [selectedDate, setSelectedDate] = useState(currentUser.date_n);

    const onUpdateInformation = async() => {

        const nameError = nameValidator(name.value);
        const phoneError = phoneValidator(phone.value);

        if(nameError || phoneError) {
            setName({ ...name, error: nameError });
            setPhone({ ...phone, error: phoneError });
            return;
        }
        setLoading(true)
        await fetch('https://benss.bensscameroun.com/api/v1/users/profile', {
            method: 'POST',
            headers:{
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            },
            body: JSON.stringify({
                    name: name.value, surname: surname.value, contact: phone.value,
                    profession: profession.value, situation: situation.value, sexe: selectedSexe
            })
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            if(res.success)
            {
                const user = {
                    id:res.id,role:res.role,name:res.name,email:res.email,_token:currentUser.toggleConnexion._token,
                    surname:res.surname,phone:res.contact,user_id:res.user_id,forfait_id:res.forfait_id,
                    lieu:res.lieu,situation:res.situation,profession:res.profession,
                }
                const action = { type: "UPDATE_PROFILE", value: user }
                dispatch(action)
                Alert.alert('Vos modifications ont été enregistreés')
            }
            else
            {
                Alert.alert('Une erreur innattendue est survenue')
            }
            
          
        })
        .catch((error) => { console.warn(error);  })


        
    }
    return (

        <View style={styles.container}>

            <View style={styles.header}>
                <HeaderNavigationBar name="Mes informations"  navigation={navigation} />
            </View>

            <View style={styles.content}> 
            <ScrollView style={{flex:1}}>
                <View style={{flex:1, marginLeft: 20,marginRight: 20}}> 

                <TextInput
                    label="Nom"
                    returnKeyType="next"
                    value={name.value}
                    onChangeText={text => setName({ value: text, error: '' })}
                    error={!!name.error}
                    errorText={name.error}
                    autoCapitalize="none"
                />

                <TextInput
                    label="Prenom"
                    returnKeyType="next"
                    value={surname.value}
                    onChangeText={text => setSurname({ value: text, error: '' })}
                    error={!!surname.error}
                    errorText={surname.error}
                    autoCapitalize="none"
                />

                <TextInput
                    label="Telephone"
                    returnKeyType="next"
                    value={phone.value}
                    onChangeText={text => setPhone({ value: text, error: '' })}
                    error={!!phone.error}
                    errorText={phone.error}
                    autoCapitalize="none"
                    keyboardType="numeric"
                />

                <TextInput
                    label="Profession"
                    returnKeyType="next"
                    value={profession.value}
                    onChangeText={text => setProfession({ value: text, error: '' })}
                    error={!!profession.error}
                    errorText={profession.error}
                    autoCapitalize="none"
                />

                <TextInput
                    label="Situation Familiale"
                    returnKeyType="next"
                    value={situation.value}
                    onChangeText={text => setSituation({ value: text, error: '' })}
                    error={!!situation.error}
                    errorText={situation.error}
                    autoCapitalize="none"
                /> 

               
                <Picker
                    selectedValue={selectedSexe ? selectedSexe : 'Homme'}
                    style={styles.picker}
                    itemStyle={styles.pickerItem}
                    onValueChange={(itemValue, itemIndex) => setSelectedSexe(itemValue)}
                >
                    <Picker.Item label="Homme" value="H" />
                    <Picker.Item label="Femme" value="F" />
                </Picker>

                { !loading  ?
                <Button mode="contained"  onPress={onUpdateInformation} >
                  Enregistrer
                </Button>
                :
                <ActivityIndicator animating={true} color='#60bdb2' />
              }
                 
               
            </View>

            </ScrollView>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
    },
    picker: {
        width: "100%",
        height: 44,
    },
    pickerItem: {
        height: 44
    }
});