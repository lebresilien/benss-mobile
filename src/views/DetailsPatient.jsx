import React, { useEffect, useState } from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Alert} from 'react-native';
import HeaderNavigationBar from '../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { List, ActivityIndicator } from 'react-native-paper';
import { useSelector} from "react-redux";

export default function DetailsPatient({ route }) {

    const navigation = useNavigation()
    const {id} = route.params
    const[user, setUser] = useState([])
    const[loading, setLoading] = useState(false)
    const currentUser = useSelector((state) => state)


    useEffect(() => {
       setUser([])
       loadData()
    },[route.params.id])

    const loadData = () => {

        setLoading(true)
        fetch('https://benss.bensscameroun.com/api/v1/users/'+id, {
            method: 'GET',
            headers:{
                'Authorization': `Bearer ${currentUser.toggleConnexion._token}`
            }
        })
        .then(res=>res.json())
        .then(res => {
            setLoading(false)
            setUser(res)
        })
        .catch((error) => { console.warn(error);  })
    }

    return (

        <View style={styles.container}>

             <View style={styles.header}>
                <HeaderNavigationBar name="details patient"  navigation={navigation}/>
            </View>

            <View style={styles.content}>
                <ScrollView style={{flex:1, paddingLeft: 50, paddingRight: 50}}>
                    { !loading ? 
                        <List.Section>
                            <List.Item title="Noms et prénoms" description={user.surname ? `${user.name} ${user.surname}` : user.name } />
                            <List.Item title="Email" description={user.email} />
                            <List.Item title="Téléphone" description={user.contact} />
                            <List.Item title="Date naissance"  description={user.dateNaisance ? user.dateNaisance: '-' } />
                            <List.Item title="Lieu naissance"  description={user.lieuNaisance ? user.lieuNaisance : '-'} />
                            <List.Item title="Prefession"  description={user.profession ? user.profession : '-'} />
                            <List.Item title="Quartier ou ville"  description={user.localisation ? user.localisation.name : '-'} />
                            <List.Item title="Forfait Souscrit"  description={user.forfait ? user.forfait.name : '-'} />
                            <List.Item title="services offerts"  description={user.forfait ? user.forfait.description : '-'} />
                            <List.Item title="Médécin"  description={user.user ? user.user.name : '-'} />
                        </List.Section>
                        :
                        <ActivityIndicator animating={true} color='#60bdb2' />
                    }
                </ScrollView>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1,
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:3,
        backgroundColor: 'white'
    },
});