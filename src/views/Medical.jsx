import React, {useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { useSelector } from "react-redux";
import HeaderNavigationBar from './../components/HeaderNavigationBar';
import { useNavigation } from '@react-navigation/native';
import { Surface } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Medical({route}) {

    const navigation = useNavigation()
    const[image, setImage] = useState({image: require('./../../assets/profil.png')})
    const currentUser = useSelector((state) => state)

    return (

        <View style={styles.container}>

             <View style={styles.header}>
                <HeaderNavigationBar name="Dossier medical"  navigation={navigation}  />
            </View>

            <View style={styles.content}>

                <Surface style={styles.surface}>
                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                      onPress={ () => navigation.navigate('DetailsPatient', {id:route.params.id})}
                    >
                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                        <Text style={styles.texte}>Profil</Text>
                    </TouchableOpacity>
                </Surface>

                <Surface style={styles.surface}>
                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}} 
                                       onPress={ () => navigation.navigate('Biometry', {id:route.params.id})}
                    >
                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                        <Text style={styles.texte}>Biometrie</Text>
                    </TouchableOpacity>
                </Surface>

                <Surface style={styles.surface}>
                    <TouchableOpacity style={{flex:1, flexDirection: 'row', alignItems: 'center'}}
                                      onPress={ () => navigation.navigate('Intervention',{id:route.params.id})}
                    >
                        <Image source={require('./../../assets/profil.png')} style={{height:100, width:100}} />
                        <Text style={styles.texte}>Interventions</Text>
                    </TouchableOpacity>
                </Surface>
            </View>



        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
    header: {
        flex:1,
        backgroundColor: '#60bdb2'
    },
    content: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    
    surface: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: -8,
        height: 60,
        width: 300,
        justifyContent: 'center',
        elevation: 4,
        borderRadius: 25,
        marginBottom:10
    },
    texte:{
        fontSize: 17,
        color: 'black',
    },
  });