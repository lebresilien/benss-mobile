import React, {useState} from 'react';
import { View, Text, StyleSheet, Image} from 'react-native';
import { TextInput } from 'react-native-paper';
import { passwordValidator, emailValidator } from '../core/utils';

export default function SignIn() {

  const[email, setEmail] = useState({ value: '', error: '' });
  const[password, setPassword] = useState({ value: '', error: '' });

  const Connexion = () => {

    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }

    alert('hh')
  };

    return (

        <View style={styles.container} >

            <View style={styles.container_description} >

                <View styles={{flex:1,}}>
                    
                     <Text style={styles.texte}>Connexion</Text>

                </View>

                <View styles={{flex:1,}}>

                    <Image style={styles.image}
                        
                        source={require('./../../assets/signin.png')}
                    />
                </View>

            </View>

            <View style={styles.container_form} >

                <TextInput
                    label="Email"
                    value={email.value}
                    onChangeText={text => setText({ value: text, error: '' })}
                    textContentType="emailAddress"
                    keyboardType="email-address"
                    error={!!email.error}
                    errorText={email.error}
                />
                  <TextInput
                    label="Mot de passe"
                    value={password.value}
                    onChangeText={text => setText({ value: text, error: '' })}
                    secureTextEntry={true}
                    placeholder="Mot de passe"
                    placeholderTextColor="#9a73ef"
                    error={!!password.error}
                    errorText={password.error}
                />
                
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    container_description: {
           flex: 1,
           justifyContent: 'space-around',
           alignItems:'center',
           backgroundColor: '#60bdb2',
           paddingTop:60,           
    },
    texte: {
        fontSize:17,
        fontWeight:'bold',
        textTransform: 'uppercase',
        color: 'white'
    },
    image: {
       width: 100,
       height: 100
    },
    container_form: {
        flex: 2,
        backgroundColor: 'white',
    }
  });