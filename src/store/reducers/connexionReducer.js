
const initialState =  {
    id:'', name:'', email:'', role:'', surname: '', phone: '', user_id: '', forfait_id: '',
    profession: '', connexion:false, _token:'', lieu:'', situation:''
}

function toggleConnexion(state = initialState, action)
{
    let nextState
    switch (action.type) {
        case 'CONNEXION':
            const temp = state.connexion
            if(temp)
            {
                nextState = {
                              ...state, 
                              connexion: false, 
                              id: '',
                              name: '',
                              surname: '',
                              phone: '',
                              profession: '',
                              email: '',
                              role: '',
                              lieu: '',
                              situation: '',
                              forfait_id: '',
                              user_id: '',
                              _token: ''
                            }            
            }else
            {
                nextState = {
                               ...state,
                               connexion: true,
                               id: action.value.id,
                               email: action.value.email,
                               role: action.value.role,
                               name: action.value.name,
                               surname: action.value.surname,
                               phone: action.value.phone,
                               profession: action.value.profession,
                               lieu: action.value.lieu,
                               situation: action.value.situation,
                               forfait_id: action.value.forfait_id,
                               user_id: action.value.user_id,
                               _token: action.value._token
                }
            }
            return nextState ;

        case 'UPDATE_PROFILE':

            nextState = {
                ...state,
                connexion: true,
                id: action.value.id,
                email: action.value.email,
                role: action.value.role,
                name: action.value.name,
                surname: action.value.surname,
                phone: action.value.phone,
                profession: action.value.profession,
                lieu: action.value.lieu,
                situation: action.value.situation,
                forfait_id: action.value.forfait_id,
                user_id: action.value.user_id,
                _token: action.value._token
            }

            return nextState || state;

        default:
            return state;
    }
}

export default toggleConnexion