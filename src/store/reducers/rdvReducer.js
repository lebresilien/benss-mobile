
const initState =  {
    date:'', heure:'', nurse_id:'', service_id:''
}

function addRDV(state = initState, action)
{
    let nextState
    switch (action.type) {
        case 'ADD-DATE':
            nextState = {
                ...state, 
                date: action.value.date,
            }
            return nextState || state;            
            
        case 'ADD-HOUR':   
            nextState = {
                ...state, 
                heure: action.value.heure,
            }
            return nextState || state;
        
        case 'ADD-NURSE':
            nextState = {
                ...state, 
                nurse_id: action.value.nurse_id,
            }
            return nextState || state;

        case 'ADD-SERVICE': 
            nextState = {
                ...state, 
                service_id: action.value.service_id,
            }
            return nextState || state;

        default:
            return state;
    }
}

export default addRDV