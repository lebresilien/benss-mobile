import { createStore, combineReducers } from 'redux';
import toggleConnexion from './reducers/connexionReducer'
import addRDV from './reducers/rdvReducer'

const rootReducer =  combineReducers({ toggleConnexion,addRDV });
export default createStore(rootReducer)

